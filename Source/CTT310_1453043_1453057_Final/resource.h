//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CTT310_1453043_1453057_Final.rc
//
#define IDC_MYICON                      2
#define IDD_CTT310_1453043_1453057_FINAL_DIALOG 102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_CTT310_1453043_1453057_FINAL 107
#define IDI_SMALL                       108
#define IDC_CTT310_1453043_1453057_FINAL 109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG                      131
#define IDC_QUERY                       1007
#define IDC_LIST1                       1008
#define IDC_NUMBER                      1010
#define IDC_TIME                        1011
#define IDC_DATAPATH                    1012
#define IDC_IMAGEPATH                   1013
#define IDC_BROWSEDATA                  1014
#define IDC_BROWSEIMG                   1015
#define IDC_BUILD                       1016
#define IDC_IMGLIST                     1018
#define IDC_STATIC1                     1019
#define IDC_LEVEL1                      1020
#define IDC_LEVEL2                      1021
#define IDC_LEVEL3                      1022
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
