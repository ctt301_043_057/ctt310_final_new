// CTT310_1453043_1453057_Final.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "CTT310_1453043_1453057_Final.h"

#define MAX_LOADSTRING 100

// Struct of Icon in ListView
struct LVIcon
{
	HICON hIcon;		// handle of Icon
	TCHAR szName[255];	// Name of Image
};

// Global Variables:
HINSTANCE hInst;                                // current instance
HWND hMainDlg = 0;
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WCHAR szDataPath[MAX_PATH];						// Database Path Folder
WCHAR szImgPath[MAX_PATH];						// Image Path 
TCHAR szDir[MAX_PATH];
WCHAR szFileName[MAX_PATH]{ 0 };
Image* selImg;									// Selected Image
int iNumImg;									// Number of pictures to show
vector <LVIcon> vectorIcon;						// Vector of Icon
HIMAGELIST hImageList;							// Handle of Image List
HWND hListControl;								// Handle of ListControl
BOOL bCompleteBuild = FALSE;							// Whether Building Database is completed


//create a vector of dtbSize images, each one has 3 Mats for 3 histogram channels 
std::vector< std::vector<cv::Mat> > dtbHistogram;
std::vector< std::vector<float> > dtbEdgeHistogram;
// name of file
std::vector<std::wstring>dtbName;
std::vector<std::wstring> dtbFileName;
//only stored 20 most relevant scores + its index in dtbFileName
//std::pair<djsd score, index>
std::vector<std::pair<double, short>> scores;
static int dtbSize = 0;

//edge filter
const float verEdgeFilter[] = { 1, -1, 1, -1 };
const float horEdgeFilter[] = { 1, 1, -1, -1 };
const float dia45EdgeFilter[] = { 1.41421, 0, 0, -1.41421 };
const float dia135EdgeFilter[] = { 0, 1.41421, -1.41421, 0 };
const float nonEdgeFilter[] = { 2, -2, -2, 2 };

// Forward declarations of functions included in this code module:
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
BOOL				InitInstance(HINSTANCE, int);
INT_PTR CALLBACK    MainDlgProc(HWND, UINT, WPARAM, LPARAM);
int					onOpenFolder();
BOOL				onOpenFile(HWND);
void				DisplayErrorBox(LPTSTR);
void				LoadImageToList(HWND hDlg, HWND hListViewWnd);
HICON				GDIPlusImageToHICON(PTSTR pFilePath);

//function: calculate d_jsd of a bin
//parameter: hm: value of a particular bin in database image
//			hmp: value of the same bin in query image
//return: d_jsd between the two bins
inline double djsd(double hm, double hmp);

vector<float> subImgEdgeHist(Mat& src, int threshold);
void querywColorHist(Mat& queryImage, UINT iNumImg);
void querywColorEdgeHist(Mat& queryImage, UINT iNumImg);
void querywColorEdgeHistLevel3(Mat& queryImage, UINT iNumImg);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_CTT310_1453043_1453057_FINAL, szWindowClass, MAX_LOADSTRING);

	// Initialize GDI+
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	selImg = NULL;

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_CTT310_1453043_1453057_FINAL));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	GdiplusShutdown(gdiplusToken);
	return (int)msg.wParam;
}


//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;
	hInst = hInstance; // Store instance handle in our global variable
	hWnd = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG), NULL, MainDlgProc);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}


// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


INT_PTR CALLBACK MainDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	// Get number of image to query 
	iNumImg = GetDlgItemInt(hDlg, IDC_NUMBER, 0, 0);
	switch (message)
	{
	case WM_INITDIALOG:
	{
		//allocate vector
		//disable Query button because we don't have the database yet
		EnableWindow(GetDlgItem(hDlg, IDC_QUERY), FALSE);
		dtbHistogram.reserve(1005);
		dtbName.reserve(1005);
		dtbFileName.reserve(1005);
		dtbEdgeHistogram.reserve(1005);

		hMainDlg = hDlg;

		// Set default level to level 1
		CheckRadioButton(hDlg, IDC_LEVEL1, IDC_LEVEL3, IDC_LEVEL3);

		SetDlgItemText(hDlg, IDC_NUMBER, L"20");
		// Handle of ListControl
		hListControl = GetDlgItem(hDlg, IDC_IMGLIST);

		return (INT_PTR)TRUE;
	}
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		switch (wmId)
		{
		case IDC_BROWSEDATA:
			bCompleteBuild = FALSE;
			onOpenFolder();
			if (szDataPath[0] != 0)
			{
				SendMessage(GetDlgItem(hDlg, IDC_DATAPATH), WM_SETTEXT, 0, (LPARAM)szDataPath);
			}
			break;
		case IDC_BROWSEIMG:
		{
			//open file dialog
			if (onOpenFile(hDlg) != 0)
			{
				SendMessage(GetDlgItem(hDlg, IDC_IMAGEPATH), WM_SETTEXT, 0, (LPARAM)szImgPath);
				// Check whether selImg is existent
				if (selImg)
				{
					delete selImg;
				}
				selImg = new Image(szImgPath);
				InvalidateRect(hDlg, NULL, TRUE);
			}
			break;
		}
		case IDC_BUILD:
			//load images in database folder and calculate histogram of each image
			//store them in dtbHistogram
		{
			// Prepare string for use with FindFile functions.  First, copy the
			// string to a buffer, then append '\*' to the directory name.
			EnableWindow(GetDlgItem(hDlg, IDC_BUILD), FALSE);
			StringCchCopy(szDir, MAX_PATH, szDataPath);
			StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

			// Find the first file in the directory.
			WIN32_FIND_DATA ffd;
			HANDLE hFind = INVALID_HANDLE_VALUE;
			hFind = FindFirstFile(szDir, &ffd);

			if (INVALID_HANDLE_VALUE == hFind)
			{
				DisplayErrorBox(TEXT("FindFirstFile"));
				return (INT_PTR)FALSE;
			}

			// establish the number of bins
			int histSize = 256;
			// Set the ranges ( for B,G,R) )
			float range[] = { 0, 256 };
			const float* histRange = { range };
			// open all the files in the directory
			do {
				//save file name to vector
				//pass directory
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					continue;

				TCHAR szPath[MAX_PATH];
				StringCchCopy(szPath, MAX_PATH, szDataPath);
				StringCchCat(szPath, MAX_PATH, TEXT("\\"));
				StringCchCat(szPath, MAX_PATH, ffd.cFileName);
				dtbName.emplace_back(ffd.cFileName);
				dtbFileName.emplace_back(std::wstring(szPath));

				//truncate the path into ascii string because imread only works with ascii string
				cv::Mat sourceImage;
				sourceImage = cv::imread(std::string(szPath, szPath + MAX_PATH), cv::IMREAD_ANYCOLOR);
				if (!sourceImage.data) {
					DisplayErrorBox(TEXT("Cannot open file."));
					FindClose(hFind);
					return (INT_PTR)FALSE;
				}

				std::vector<cv::Mat> bgr_planes;
				cv::split(sourceImage, bgr_planes);

				std::vector<cv::Mat> v(3, cv::Mat(1, 256, CV_32F));

				/// Compute the histograms:
				calcHist(&bgr_planes[0], 1, 0, cv::Mat(), v[0], 1, &histSize, &histRange, true, false);
				calcHist(&bgr_planes[1], 1, 0, cv::Mat(), v[1], 1, &histSize, &histRange, true, false);
				calcHist(&bgr_planes[2], 1, 0, cv::Mat(), v[2], 1, &histSize, &histRange, true, false);

				cv::normalize(v[0], v[0], 1.0, 0.01, NORM_MINMAX);
				cv::normalize(v[1], v[1], 1.0, 0.01, NORM_MINMAX);
				cv::normalize(v[2], v[2], 1.0, 0.01, NORM_MINMAX);
				dtbHistogram.emplace_back(v);

				dtbEdgeHistogram.emplace_back(subImgEdgeHist(sourceImage, 5));

				dtbSize++;
			} while (FindNextFile(hFind, &ffd) != 0);

			DWORD dwError = GetLastError();
			if (dwError != ERROR_NO_MORE_FILES)
			{
				DisplayErrorBox(TEXT("FindFirstFile"));
			}

			FindClose(hFind);
			//std::string test1 = std::to_string(dtbHistogram.size());
			MessageBoxA(NULL, "Building database done.", "Info", MB_OK);
			EnableWindow(GetDlgItem(hDlg, IDC_QUERY), TRUE);
			bCompleteBuild = TRUE;
			break;
		}
		case IDC_QUERY:
		{
			// Empty ListControl
			ListView_DeleteAllItems(hListControl);
			// Clear Vector Icon
			vectorIcon.clear();

			UINT iNumImg = GetDlgItemInt(hDlg, IDC_NUMBER, NULL, FALSE);
			if (iNumImg == 0 || iNumImg > dtbSize)
				iNumImg = 20;

			//open query image
			cv::Mat queryImage;
			queryImage = cv::imread(std::string(szImgPath, szImgPath + MAX_PATH), cv::IMREAD_ANYCOLOR);
			if (!queryImage.data) {
				DisplayErrorBox(TEXT("Cannot open query file"));
				return (INT_PTR)FALSE;
			}

			auto bf = std::chrono::high_resolution_clock::now();
			if (IsDlgButtonChecked(hDlg, IDC_LEVEL1) == BST_CHECKED) {
				querywColorHist(queryImage, iNumImg);
			}
			else if (IsDlgButtonChecked(hDlg, IDC_LEVEL2) == BST_CHECKED) {
				querywColorEdgeHist(queryImage, iNumImg);
			}
			else if (IsDlgButtonChecked(hDlg, IDC_LEVEL3) == BST_CHECKED) {
				querywColorEdgeHistLevel3(queryImage, iNumImg);
			}
		

			auto af = std::chrono::high_resolution_clock::now();
			std::ostringstream stream;
			stream << std::chrono::duration_cast<std::chrono::microseconds>(af - bf).count() << " microsec";
			const std::string& tmp = stream.str();
			SendMessageA(GetDlgItem(hDlg, IDC_TIME), WM_SETTEXT, 0, (LPARAM)tmp.c_str());

			// Add all imgae to vectorIcon
			for (size_t i = 0; i < scores.size(); i++)
			{
				// push image to vector
				LVIcon listViewIcon;
				listViewIcon.hIcon = GDIPlusImageToHICON((PTSTR)dtbFileName[scores[i].second].c_str());
				// set name for icon
				/*listViewIcon.szName[0] = '\0';*/
				StringCchCopy(listViewIcon.szName, MAX_PATH,dtbName[scores[i].second].c_str());
				vectorIcon.push_back(listViewIcon);
			}
			LoadImageToList(hDlg, hListControl);
			break;
		}
		}
		break;
	}
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hDlg, &ps);
		// Paint to Picture Control 
		// Not actually =)))))
		Gdiplus::Rect destRect(11, 11, 230, 168);
		if (selImg)
		{
			Graphics graphics(hdc);
			graphics.DrawImage(selImg, destRect);
		}
		EndPaint(hDlg, &ps);
		break;
	}
	case WM_CLOSE:
		if (selImg)
			delete selImg;
		EndDialog(hDlg, LOWORD(wParam));
		PostQuitMessage(0);
		return (INT_PTR)TRUE;
	}
	return (INT_PTR)FALSE;
}


int onOpenFolder()
{
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED |
		COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{
		IFileOpenDialog *pFileOpen;

		// Create the FileOpenDialog object.
		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL,
			IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));

		if (SUCCEEDED(hr))
		{
			DWORD dwOptions;
			if (SUCCEEDED(pFileOpen->GetOptions(&dwOptions)))
			{
				pFileOpen->SetOptions(dwOptions | FOS_PICKFOLDERS);
			}
			// Show the Open dialog box.
			hr = pFileOpen->Show(NULL);

			// Get the file name from the dialog box.
			if (SUCCEEDED(hr))
			{
				IShellItem *pItem;
				hr = pFileOpen->GetResult(&pItem);
				if (SUCCEEDED(hr))
				{
					PWSTR pszFilePath;
					hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

					if (SUCCEEDED(hr))
					{
						StringCchCopy(szDataPath, MAX_PATH, pszFilePath);
						CoTaskMemFree(pszFilePath);
					}
					pItem->Release();
				}
			}
			pFileOpen->Release();
		}
		CoUninitialize();
	}
	return 0;
}

BOOL onOpenFile(HWND hWnd)
{
	OPENFILENAME ofn;

	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szImgPath;
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrFileTitle = szFileName;
	ofn.nMaxFileTitle = MAX_PATH;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	return GetOpenFileName(&ofn);
}

void DisplayErrorBox(LPTSTR lpszFunction)
{
	// Retrieve the system error message for the last-error code

	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	// Display the error message and clean up

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
	StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error %d: %s"),
		lpszFunction, dw, lpMsgBuf);
	MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}

inline double djsd(double hm, double hmp)
{
	//if (hm == 0) hm = 1;
	//if (hmp == 0) hmp = 1;
	return hm * log2(2 * hm / (hm + hmp)) + hmp * log2(2 * hmp / (hm + hmp));
}

vector<float> subImgEdgeHist(Mat & src, int threshold)
{
	// subimage size
	const int subHeight = src.rows / 4; //120
	const int subWidth = src.cols / 4; //160
	const int desireNumBlock = 12;
	const int blockSize = sqrt((subHeight * subWidth) / desireNumBlock) / 2; //20

	vector<float> edgeHist(80, 1.0);

	//luminance mean value for subblocks of a sub-image
	vector<vector<float>> lumiMeanVal(6, vector<float>(8, 0));

	int currentBin = 0;
	for (int img_r = 0; img_r < src.rows; img_r += subHeight)
	{
		for (int img_c = 0; img_c < src.cols; img_c += subWidth)
		{
			//reset lumiMeanVal
			//for (auto& i : lumiMeanVal)
			//	fill(i.begin(), i.end(), 0);
			for (int block_r = img_r, ii = 0; block_r < img_r + subHeight; block_r += blockSize, ii++) {
				for (int block_c = img_c, jj = 0; block_c < img_c + subWidth; block_c += blockSize, jj++) {
					float sumLumi = 0;
					//calculate mean luminance of a 20x20 block
					//close-enough luminance of a RGB pixel: Y = 0.33 R + 0.5 G + 0.16 B : (R+R+B+G+G+G)/6
					for (int i = block_r; i < block_r + blockSize; i++) {
						for (int j = block_c; j < block_c + blockSize; j++) {
							sumLumi = sumLumi + (src.at<Vec3b>(i, j)[0] + 3 * src.at<Vec3b>(i, j)[1]
								+ 2 * src.at<Vec3b>(i, j)[2]) / 6;
						}
					}
					lumiMeanVal[ii][jj] = sumLumi / (blockSize*blockSize);
				}
			}

			//convol with 5 edge filter and decide which kind of edge this 40x40 block has
			for (int m = 0; m < 6; m += 2) {
				for (int n = 0; n < 8; n += 2) {
					char max_index = 0;
					float max = lumiMeanVal[m][n] * verEdgeFilter[0] + lumiMeanVal[m][n + 1] * verEdgeFilter[1] +
						lumiMeanVal[m + 1][n] * verEdgeFilter[2] + lumiMeanVal[m + 1][n + 1] * verEdgeFilter[3];

					float b2 = lumiMeanVal[m][n] * horEdgeFilter[0] + lumiMeanVal[m][n + 1] * horEdgeFilter[1] +
						lumiMeanVal[m + 1][n] * horEdgeFilter[2] + lumiMeanVal[m + 1][n + 1] * horEdgeFilter[3];
					if (b2 > max) {
						max = b2; max_index = 1;
					}
					float b3 = lumiMeanVal[m][n] * dia45EdgeFilter[0] + lumiMeanVal[m][n + 1] * dia45EdgeFilter[1] +
						lumiMeanVal[m + 1][n] * dia45EdgeFilter[2] + lumiMeanVal[m + 1][n + 1] * dia45EdgeFilter[3];
					if (b3 > max) {
						max = b3; max_index = 2;
					}
					float b4 = lumiMeanVal[m][n] * dia135EdgeFilter[0] + lumiMeanVal[m][n + 1] * dia135EdgeFilter[1] +
						lumiMeanVal[m + 1][n] * dia135EdgeFilter[2] + lumiMeanVal[m + 1][n + 1] * dia135EdgeFilter[3];
					if (b4 > max) {
						max = b4; max_index = 3;
					}
					float b5 = lumiMeanVal[m][n] * nonEdgeFilter[0] + lumiMeanVal[m][n + 1] * nonEdgeFilter[1] +
						lumiMeanVal[m + 1][n] * nonEdgeFilter[2] + lumiMeanVal[m + 1][n + 1] * nonEdgeFilter[3];
					if (b4 > max) {
						max = b5; max_index = 4;
					}
					if (max > threshold)
						edgeHist[currentBin + max_index]++;
				}
			}
			currentBin += 5;
		}
	}

	//normalize edge histogram
	for (auto i = edgeHist.begin(); i != edgeHist.end(); i += 5) {
		float numEdge = std::accumulate(i, i+5, 0.0);
		for (auto e = i; e != i + 5; e++)
			*e = (*e) / numEdge;
	}

	return edgeHist;
}


//// Load Image To ListControl 
void LoadImageToList(HWND hDlg, HWND hListViewWnd)
{
	// Create size of each Icon to be shown
	hImageList = ImageList_Create(GetSystemMetrics(SM_CXICON) + 110,
		GetSystemMetrics(SM_CYICON) + 90, ILC_MASK | ILC_COLOR32, 1, 1);

	// Add all Icon to ImageList
	for (int i = 0; i < vectorIcon.size(); ++i)
	{
		ImageList_AddIcon(hImageList, vectorIcon[i].hIcon);
		ListView_SetIconSpacing(hListViewWnd, 155, 155);
	}

	// Set Image List
	ListView_SetImageList(hListViewWnd, hImageList, LVSIL_NORMAL);
	LVITEM lvi = { 0 };
	lvi.mask = LVIF_IMAGE | LVIF_TEXT;

	for (int i = 0; i < vectorIcon.size(); ++i)
	{
		lvi.iItem = i;
		lvi.iImage = i;
		lvi.pszText = (LPWSTR) vectorIcon[i].szName;
		SendMessage(hListViewWnd, LVM_INSERTITEM, 0, (LPARAM)&lvi);
	}
}

// Convert Gdiplus::Image to HICON from path of file
HICON GDIPlusImageToHICON(PTSTR pFilePath)
{
	HICON hIcon = NULL;
	HBITMAP hBmp = NULL;
	Image* tempImg = Image::FromFile(pFilePath, false);

	// cast Image to Bitmap
	Bitmap* bmp = static_cast<Bitmap*>(tempImg);
	if (bmp)
	{
		bmp->GetHICON(&hIcon);
		delete bmp;
	}
	return hIcon;
}

void querywColorHist(Mat& queryImage, UINT iNumImg)
{
	//calculate histogram of query image
	// establish the number of bins
	int histSize = 256;
	// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 };
	const float* histRange = { range };

	std::vector<cv::Mat> bgr_planes;
	cv::split(queryImage, bgr_planes);

	cv::Mat bq_hist, gq_hist, rq_hist;

	/// Compute the histograms:
	calcHist(&bgr_planes[0], 1, 0, cv::Mat(), bq_hist, 1, &histSize, &histRange, true, false);
	calcHist(&bgr_planes[1], 1, 0, cv::Mat(), gq_hist, 1, &histSize, &histRange, true, false);
	calcHist(&bgr_planes[2], 1, 0, cv::Mat(), rq_hist, 1, &histSize, &histRange, true, false);

	//normalize histogram
	cv::normalize(bq_hist, bq_hist, 1.0, 0.01, NORM_MINMAX);
	cv::normalize(gq_hist, gq_hist, 1.0, 0.01, NORM_MINMAX);
	cv::normalize(rq_hist, rq_hist, 1.0, 0.01, NORM_MINMAX);

	//calculate d_jsd between query image and each image in database
	//only stored iNumImg most relevant scores + its index in dtbFileName
	//std::pair<djsd score, index>
	//std::vector<std::pair<double, short>> scores;
	scores.clear();
	scores.reserve(dtbSize);
	double maxScore = -1;
	auto maxIterator = scores.begin(); //iterator to the highest score in the scores vector
									   //loop through dtbHistogram vector
	for (short index = 0; index < dtbHistogram.size(); ++index) {
		double score = 0;
		for (int i = 0; i < 256; i++) {
			score = score + djsd(dtbHistogram[index][0].at<float>(i),
				bq_hist.at<float>(i));
		}
		for (int i = 0; i < 256; i++) {
			score = score + djsd(dtbHistogram[index][1].at<float>(i),
				bq_hist.at<float>(i));
		}
		for (int i = 0; i < 256; i++) {
			score = score + djsd(dtbHistogram[index][2].at<float>(i),
				bq_hist.at<float>(i));
		}

		scores.emplace_back(make_pair(score, index));
	}

	//sort scores vector based on score
	std::sort(scores.begin(), scores.end());
	scores.resize(iNumImg);
}
void querywColorEdgeHist(Mat& queryImage, UINT iNumImg)
{
	//calculate histogram of query image--------------------------------------------
	// establish the number of bins
	int histSize = 256;
	// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 };
	const float* histRange = { range };

	std::vector<cv::Mat> bgr_planes;
	cv::split(queryImage, bgr_planes);

	cv::Mat bq_hist, gq_hist, rq_hist;

	/// Compute the histograms:
	calcHist(&bgr_planes[0], 1, 0, cv::Mat(), bq_hist, 1, &histSize, &histRange, true, false);
	calcHist(&bgr_planes[1], 1, 0, cv::Mat(), gq_hist, 1, &histSize, &histRange, true, false);
	calcHist(&bgr_planes[2], 1, 0, cv::Mat(), rq_hist, 1, &histSize, &histRange, true, false);

	//normalize histogram
	cv::normalize(bq_hist, bq_hist, 1.0, 0.01, NORM_MINMAX);
	cv::normalize(gq_hist, gq_hist, 1.0, 0.01, NORM_MINMAX);
	cv::normalize(rq_hist, rq_hist, 1.0, 0.01, NORM_MINMAX);


	//calculate local edge histogram of query image---------------------------------
	vector<float> qEdgeHist = subImgEdgeHist(queryImage, 5);

	//calculate d_jsd between query image and each image in database
	//only stored iNumImg most relevant scores + its index in dtbFileName
	//std::pair<djsd score, index>
	//std::vector<std::pair<double, short>> scores;
	scores.clear();
	scores.reserve(dtbSize);
	double maxScore = -1;
	auto maxIterator = scores.begin(); //iterator to the highest score in the scores vector

	//loop through dtbHistogram vector and dtbEdgeHist
	for (short index = 0; index < dtbHistogram.size(); ++index) {
		double score = 0;
		for (int i = 0; i < 256; i++) {
			score = score + djsd(dtbHistogram[index][0].at<float>(i),
				bq_hist.at<float>(i));
		}
		for (int i = 0; i < 256; i++) {
			score = score + djsd(dtbHistogram[index][1].at<float>(i),
				gq_hist.at<float>(i));
		}
		for (int i = 0; i < 256; i++) {
			score = score + djsd(dtbHistogram[index][2].at<float>(i),
				rq_hist.at<float>(i));
		}
		for (int i = 0; i < qEdgeHist.size(); i++) {
			score += djsd(qEdgeHist[i], dtbEdgeHistogram[index][i]);
		}

		scores.emplace_back(make_pair(score, index));
	}

	//sort scores vector based on score
	std::sort(scores.begin(), scores.end());
	scores.resize(iNumImg);
}

void querywColorEdgeHistLevel3(Mat& queryImage, UINT iNumImg)
{
	//calculate histogram of query image--------------------------------------------
	// establish the number of bins
	int histSize = 256;
	// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 };
	const float* histRange = { range };

	std::vector<cv::Mat> bgr_planes;
	cv::split(queryImage, bgr_planes);

	cv::Mat bq_hist, gq_hist, rq_hist;

	/// Compute the histograms:
	calcHist(&bgr_planes[0], 1, 0, cv::Mat(), bq_hist, 1, &histSize, &histRange, true, false);
	calcHist(&bgr_planes[1], 1, 0, cv::Mat(), gq_hist, 1, &histSize, &histRange, true, false);
	calcHist(&bgr_planes[2], 1, 0, cv::Mat(), rq_hist, 1, &histSize, &histRange, true, false);

	//normalize histogram
	cv::normalize(bq_hist, bq_hist, 1.0, 0.01, NORM_MINMAX);
	cv::normalize(gq_hist, gq_hist, 1.0, 0.01, NORM_MINMAX);
	cv::normalize(rq_hist, rq_hist, 1.0, 0.01, NORM_MINMAX);


	//calculate local edge histogram of query image---------------------------------
	vector<float> qEdgeHist = subImgEdgeHist(queryImage, 5);

	//calculate d_jsd between query image and each image in database
	//only stored iNumImg most relevant scores + its index in dtbFileName
	//std::pair<djsd score, index>
	//std::vector<std::pair<double, short>> scores;
	scores.clear();
	scores.reserve(dtbSize);
	double maxScore = -1;
	auto maxIterator = scores.begin(); //iterator to the highest score in the scores vector

	//loop through dtbHistogram vector and dtbEdgeHist
	for (short index = 0; index < dtbHistogram.size(); ++index) {
		double score = 0;
		for (int i = 0; i < 256; i++) {
			score = score + djsd(dtbHistogram[index][0].at<float>(i),
				bq_hist.at<float>(i));
		}
		for (int i = 0; i < 256; i++) {
			score = score + djsd(dtbHistogram[index][1].at<float>(i),
				gq_hist.at<float>(i));
		}
		for (int i = 0; i < 256; i++) {
			score = score + djsd(dtbHistogram[index][2].at<float>(i),
				rq_hist.at<float>(i));
		}
		for (int i = 0; i < qEdgeHist.size(); i++) {
			score += djsd(qEdgeHist[i], dtbEdgeHistogram[index][i]);
		}
		scores.emplace_back(make_pair(score, index));
	}

	//sort scores vector based on score
	std::sort(scores.begin(), scores.end());
	scores.resize(iNumImg);
}