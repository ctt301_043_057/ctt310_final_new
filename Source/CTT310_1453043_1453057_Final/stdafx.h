// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// TODO: reference additional headers your program requires here
#include <commdlg.h>
#include <Shobjidl.h>
#include <Strsafe.h> //for StringCchCopy
#include <Shlwapi.h> //for PathFileExists
#include <vector>
#include <string>
#include <utility>
#include <chrono>
#include <array>
#include <numeric>

//opencv libraries
#include "Libraries\Headers\opencv2\core\core.hpp"
#include "Libraries\Headers\opencv2\highgui\highgui.hpp"
#include "Libraries\Headers\opencv2\imgproc\imgproc.hpp"

#include <Unknwn.h> 
#include <algorithm>
#include <numeric>
#include <minmax.h>
#include <gdiplus.h>
#include <objidl.h>
#include <mmsystem.h>
#include <commdlg.h>
#include <commctrl.h>
#pragma comment(lib,"Winmm.lib")
#pragma comment(lib, "comctl32.lib")
#pragma comment (lib,"Gdiplus.lib")
#include <math.h>
#include <shellapi.h>
#include <stdlib.h>

using namespace std;
using namespace Gdiplus;
using namespace cv;